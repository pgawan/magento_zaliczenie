<?php

namespace Apsl\StoreLocator\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class Store extends AbstractModel implements IdentityInterface
{
    /**
     * @var string
     */
    const CACHE_TAG = 'apsl_storelocator_store';

    /**
     * @var string
     */
    const STORE_REGISTRY_KEY = 'apslCurrentSotre';

    protected function _construct()
    {
        $this->_init("Apsl\StoreLocator\Model\ResourceModel\Store");
    }

    /**
     * @return array|string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId(), self::STORE_REGISTRY_KEY];
    }
}