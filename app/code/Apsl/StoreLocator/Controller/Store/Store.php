<?php

namespace Apsl\StoreLocator\Controller\Store;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Apsl\StoreLocator\Model\StoreFactory;
use Apsl\StoreLocator\Model\ResourceModel\Store as StoreResource;

class Store extends Action
{
    /** @var PageFactory */
    private $resultPageFactory;

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var StoreFactory
     */
    private $storeModel;

    /**
     * @var StoreResource
     */
    private $storeResource;

//    /**
//     * Stores constructor.
//     * @param Context $context
//     * @param PageFactory @resultPageFactory
//     */

    /**
     * Store constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param Registry $registry
     * @param StoreFactory $storeModel
     * @param StoreResource $storeResource
     */
    public function __construct(Context $context, PageFactory $resultPageFactory, Registry $registry, StoreFactory $storeModel, StoreResource $storeResource)
    {
        parent::__construct($context);

        $this->resultPageFactory = $resultPageFactory;
        $this->registry = $registry;
        $this->storeModel = $storeModel;
        $this->storeResource = $storeResource;
    }


    public function execute()
    {
//     var_dump('Stores actions');die;
        $request = $this->getRequest();
        $storeId = $request->getParam('id', null);

        if(is_null($storeId)) {
            $this->_forward('noRoute');
            return;
        }

        /**
         * @var \Apsl\StoreLocator\Model\Store $storeModel
         */
        $storeModel = $this->storeModel->create();
        $this->storeResource->load($storeModel, $storeId);


        $this->registry->register(\Apsl\StoreLocator\Model\Store::STORE_REGISTRY_KEY, $storeModel);


        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }
}