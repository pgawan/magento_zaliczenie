<?php

namespace Apsl\StoreLocator\Block;

use Magento\Framework\View\Element\Template;
use Apsl\StoreLocator\Model\ResourceModel\Store\CollectionFactory as StoresFactory;

/**
 * Class StoreList
 * @package Apsl\StoreLocator\Block
 */
class StoreList extends Template
{
    private $storeList;
//
    public function __construct(Template\Context $context, StoresFactory $storeList, array $data = [])
    {
        parent::__construct($context, $data);
        $this->storeList = $storeList;
    }



    /**
     * @return Collection
     */
    public function getAvailableStore() {
        /**
         * @var Collection $stores
         */
        $stores = $this->storeList->create();

        return $stores;
    }

    /**
     * @return string
     */
    public function getSomething()
    {
        return 'something';
    }
}